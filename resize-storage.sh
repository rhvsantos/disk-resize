#!/bin/bash
# By Rafael Santos (04/07/22)
# This script checks and resizes the partition and Filesystem of image's mountpoint (/storage or /storagedb).

source "$(dirname $0)/mountpoints"
_telegram_chat_id="CHAT_ID_HERE"
_telegram_botid="BOT_ID_HERE"
_log_file="/var/log/disk-resize.log"
_time=$(date +"%d/%m/%Y - %H:%M:%S")
_free_space_warn="8000000"
_log_size_warn="2599629"
_app_port="8443/tcp"
_telegram_api_url="https://api.telegram.org/bot$_telegram_botid/sendMessage"


function _get_disk() {

	echo "- Capturando informações necessárias." | tee -a "$_log_file"
	_get_source=$(sed -n 2p <(findmnt "$_real_mountpoint") | awk '{print$2}')
	_get_fs=$(sed -n 2p <(findmnt "$_real_mountpoint") | awk '{print$3}')
	
	if dmsetup info "$_get_source" &> /dev/null
	then
		_is_lvm=true
	else
		_is_lvm=false
	fi

	if [ "$_is_lvm" == "true" ]
	then
		_get_vg_name=$(sed -n 2p <(lvs "$_get_source") | awk '{print$2}')
		_disks=$(grep "$_get_vg_name " <(pvscan) | awk '{print$2}')
		_pv_amount=$(wc -w <<< $_disks)

		if [ "$_pv_amount" -gt 1 ]
		then
			_additional_msg="$_additional_msg

Este ponto de montagem utiliza $_pv_amount discos para o LVM (Solicitar aumento no menor disco!) :
"
		fi

	else
		_disks="$_get_source"
	fi

	for _targets in $_disks
	do

		if grep "[1-9]" <<< "$_targets" &> /dev/null
		then
			is_partition="yes"
			_partition_number=$(grep -o [1-9] <<< $_targets)
			_real_disk_size=$(sed -n 2p <(lsblk ${_targets%[1-9]}) | awk '{print$4}')
			
			_additional_msg="$_additional_msg
$_real_mountpoint: Disco de $_real_disk_size , o disco utiliza a partição $_partition_number para este ponto de montagem!"
		else
			is_partition="no"
			_real_disk_size=$(sed -n 2p <(lsblk ${_targets%[1-9]}) | awk '{print$4}')
			
			_additional_msg="$_additional_msg
$_real_mountpoint: Disco de $_real_disk_size , o disco não está particionado!"
		fi

	done

}

function _sep() {
    echo "************************************"
}

function _log_size() {

	_log_size=$(stat -c %s "$_log_file")
	_log_size_int=$(($_log_size/1))

	if [ $_log_size_int -gt $_log_size_warn ] ;then
		echo "--------------- Log was erased ---------------" > "$_log_file"
	fi

}

function _send_msg() {


	curl -X POST -H 'Content-Type: application/json' -d '{"chat_id": '"$_telegram_chat_id"', "text": "'"$_telegram_message"'"}' $_telegram_api_url
	#curl -v -F "chat_id=$_telegram_chat_id" -F document=@"$_log_file" $_telegram_api_url

}

# Detect what is the mountpoint that is being used (of $_mount_point) and it`s filesystem
function _discover() {
        echo "- Identificando filesystem e device..." | tee -a "$_log_file"

        for _mnt in "${_mount_point[@]}"
        do
			echo "++ [exec] findmnt $_mnt &> /dev/null" | tee -a "$_log_file"
            findmnt "$_mnt" &> /dev/null

            [ "$?" -eq 0 ] && { 
                _real_mountpoint=$_mnt
                _filesystem=$(sed -n 2p <(findmnt "$_mnt") | awk '{print$3}')
                _dev=$(sed -n 2p <(findmnt "$_mnt") | awk '{print$2}')
                _free_size=$(awk '{print$4}' <(df -h $_mnt) | sed -n 2p)
                break
            }

        done
}

# Detect if it is a LVM member
function _lvm_discover() {
        echo "- Identificando se o mountpoint $_mnt faz parte de um LV" | tee -a "$_log_file"

		echo "++ [exec] dmsetup info ${_dev##*/} &> /dev/null" | tee -a "$_log_file"
        dmsetup info ${_dev##*/} &> /dev/null
		

        [ "$?" -eq 0 ] && { _lvm=true; } || { _lvm=false; }

        echo "LVM: $_lvm" | tee -a "$_log_file"

        [ "$_lvm" == "true" ] && {

            # Detect Pysical Volume member
	        _lvm_pv=$(grep "$_dev" <(lvs -o lv_dm_path,devices) | awk '{print$2}' | sed "s/([0-9])//" | sed -E "s/\(.*\)//" | uniq)
            _lvm_pv_amount=$(wc -w <<< $_lvm_pv)

            _error_pv=${_lvm_pv:?"Pysical Volume não encontrado."}

            echo "Quantidade de devices no VG: $_lvm_pv_amount" | tee -a "$_log_file"
            echo "Devices participantes do VG:" $_lvm_pv | tee -a "$_log_file"

            _dev_orig="$_dev"
            _dev="$_lvm_pv"

        }
}


# Send a command to the kernel check the new space
function _kernel_new_space() {
        echo -n "- Identificando novo espaço no(s) disco(s) pelo Kernel" | tee -a "$_log_file"

        for _kernel_dev in ${_dev[@]} 
        do
            _dev_no_path=${_kernel_dev##*/}
            echo 1 > "/sys/class/block/${_dev_no_path/[0-9]/}/device/rescan"
			echo "++ [exec] echo 1 > /sys/class/block/${_dev_no_path/[0-9]/}/device/rescan" | tee -a "$_log_file"
        done

        echo " [ OK ] "
}

# Checking if it the new space is available
function _check_new_space() {

        echo "- Verificando se novo espaço foi reconhecido pelo kernel." | tee -a "$_log_file"
		echo "++ [exec] grep -q sd[a-z]: detected capacity change.* /var/log/kern.log" | tee -a "$_log_file"
        grep -q "sd[a-z]: detected capacity change.*" /var/log/kern.log

        [ "$?" -ne 0 ] && {

            echo "Nenhum novo espaço foi reconhecido no disco." | tee -a "$_log_file"
	    _get_disk
	    _check_space
        } ||
	{
	    echo "Novo espaço foi reconhecido!" | tee -a "$_log_file"
	    sed -i "s/sd[a-z]: detected capacity change.*/Reconhecido pelo script resize-storage.sh/" /var/log/kern.log
		echo "++ [exec] sed -i 's/sd[a-z]: detected capacity change.*/Reconhecido pelo script resize-storage.sh/'" | tee -a "$_log_file"
	}
}

function _check_space() {

	echo -n "- Checando espaço disponível no $_real_mountpoint..." | tee -a "$_log_file"
	_space_now=$(awk '{print$4}' <(df "$_real_mountpoint") | sed -n 2p)
	_space_integer=$(($_space_now/1))

	if [ "$_space_integer" -lt "$_free_space_warn" ] ;then
		echo "Enviando mensagem para Telegram." | tee -a "$_log_file"

		if ! fuser -s "$_app_port"
		then
			export _telegram_message="$(hostname) ($(hostname -I))
			
			CentralPK parada por falta de espaço!
			
			Espaço atual: $(df -h "$_real_mountpoint" | sed -n 2p | awk '{print$4}')
			
			$_additional_msg"

		else
			export _telegram_message="$_real_mountpoint do Host $(hostname) ($(hostname -I)) está com pouco espaço livre.
		
		Espaço livre atual: $(df -h "$_real_mountpoint" | sed -n 2p | awk '{print$4}')
		
			$_additional_msg"

		fi

		_send_msg

	else
		echo " [ OK ]"
	fi
        
	exit 1
}

# Resize the partition (If it is a partition)
function _resize_partition() {
        for _partition in ${_dev[@]}
        do
            _partition_number=${_partition##*[a-z]}

            [ -n "$_partition_number" ] && { 
                echo "- Realizando aumento da partição $_partition_number do disco ${_partition/[0-9]/}" | tee -a "$_log_file"
                eval growpart ${_partition/[0-9]/} $_partition_number;
				echo "++ [exec] eval growpart ${_partition/[0-9]/} $_partition_number" | tee -a "$_log_file"
            }

        done
}

# If it is an LV, resize PV and LV
function _resize_lvm() {
        [ "$_lvm" == "true" ] && {
	    echo "- Aumentando PV e LV." | tee -a "$_log_file"

	    for _pv in ${_dev[@]}
            do
            	pvresize "$_pv"
				echo "++ [exec] pvresize $_pv" | tee -a "$_log_file"
	    done
            
	    lvresize -l +100%FREE "$_dev_orig"
		echo "++ [exec] lvresize -l +100%FREE $_dev_orig" | tee -a "$_log_file"
        }
}

# Filesystem Resize
function _resize_fs() {
	echo "- Realizando resize do Filesystem." | tee -a "$_log_file"
	case $_filesystem in

		btrfs)
				btrfs fi resize max "$_real_mountpoint"
				echo "++ [exec] btrfs fi resize max $_real_mountpoint" | tee -a "$_log_file"
			;;
			
		xfs)
				xfs_growfs "$_real_mountpoint"
				echo "++ [exec] xfs_growfs $_real_mountpoint" | tee -a "$_log_file"
				;;
		ext4)
				resize2fs "$_dev_orig"
				echo "++ [exec] resize2fs $_dev_orig" | tee -a "$_log_file"
				;;
	esac
		
		_new_free_size=$(awk '{print$4}' <(df -h $_mnt) | sed -n 2p)

}

# ------------------- INICIO ---------------------------- #

[ "$UID" -ne 0 ] && { echo "You need to be root to execute this script!"; exit 1; }

_log_size

echo $_time | tee -a "$_log_file"

_sep

_discover

_error_fs={_filesystem:?"Filesystem não encontrado"}
_error_dev={_dev:?"Device não encontrado"}

echo "[Filesystem]: $_filesystem" | tee -a "$_log_file"
echo "[Mountpoint]: $_real_mountpoint" | tee -a "$_log_file"
echo "[Device]: $_dev" | tee -a "$_log_file"

_sep

_lvm_discover

_sep

_kernel_new_space

_sep

_check_new_space

_sep

_resize_partition

_sep

_resize_lvm

_sep

_resize_fs

_sep

[ "$_free_size" != "$_new_free_size" ] && { 
	
	# Message telling that the mountpoint was resized automatically

	if fuser -s "$_app_port"
	then

		export _telegram_message="$_real_mountpoint do Host $(hostname) ($(hostname -I)) foi aumentado automaticamente.
	
		Espaço livre (Antes): $_free_size
		Espaço livre (Depois): $_new_free_size"
	else
		export _telegram_message="$_real_mountpoint do Host $(hostname) ($(hostname -I)) foi aumentado automaticamente.
	
		Espaço livre (Antes): $_free_size
		Espaço livre (Depois): $_new_free_size
		
		*** Obs.: Será necessário subir a aplicação CentralPK ***"

	fi	
	
	_send_msg 
}

echo "Espaço livre antes ($_real_mountpoint): $_free_size" | tee -a "$_log_file"
echo "Espaço livre agora ($_real_mountpoint): $_new_free_size" | tee -a "$_log_file"

echo '----------------- EOF -----------------'| tee -a "$_log_file"
echo

exit 0
