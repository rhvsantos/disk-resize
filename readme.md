The activity of resizing Partitions and Filesystems is very common in our work routine, because of this, I've made this script to help us to 
do this. Basically, we have more than 30 servers, each one with a type of Filesystem and sometimes without a pattern for the mountpoint name. So, this script identify the right mountpoint
that need to be resized (/storage_images or /storage), identify if the mountpoint is a LVM member (if it is, make extra steps to resize PV and LV) and at least, identify and resize the filesystem.

Those mountpoint that I've mentioned, grow very fast sometimes (because all the images and some videos are stored into), and some servers are configured to use the /storage mountpoint and others /storage_images, and the filesystem are different too. So, this script is Universal.

You can put this script into the crontab, and when the kernel detect the space difference, all the steps are executed. Additionally, I've configured to send messages in our telegram group (Using Telegram Bot API)

